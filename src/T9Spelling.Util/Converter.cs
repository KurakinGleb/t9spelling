﻿namespace T9Spelling.Util
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Models;
    using Tools;

    /// <summary>
    ///     Converter for encrypting and decrypting messages.
    /// </summary>
    public class Converter
    {
        private const char SeparatorChar = ' ';
        private readonly Keypad keypad;

        /// <summary>
        ///     Initializes converter for encrypting and decrypting messages.
        /// </summary>
        /// <param name="keypad">Keypad for converter work.</param>
        public Converter(Keypad keypad)
        {
            this.keypad = keypad;
        }

        /// <summary>
        ///     Gets decrypted input message.
        /// </summary>
        /// <param name="message">Message to decrypt.</param>
        /// <returns>Result of decrypting input message.</returns>
        public string DecryptMessage(string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                return string.Empty;
            }

            var result = new StringBuilder();
            var numberOfButtonClicks = 0;
            for (var i = 0; i < message.Length; i++)
            {
                var currentCode = message[i];
                var button = keypad.GetButtonByKey(currentCode);
                numberOfButtonClicks++;

                if (i == message.Length - 1 || currentCode != message[i + 1])
                {
                    if (button != null)
                    {
                        var letter = button.Value[numberOfButtonClicks - 1];
                        result.Append(letter);
                    }
                    else if (currentCode != SeparatorChar)
                    {
                        throw new ArgumentException($"Unknown digit : '{currentCode}'.");
                    }

                    numberOfButtonClicks = 0;
                }
            }

            return result.ToString();
        }

        /// <summary>
        ///     Gets decrypted input messages.
        /// </summary>
        /// <param name="messages">Messages to decrypt.</param>
        /// <returns>Result of decrypting input messages.</returns>
        public List<string> DecryptMessages(List<string> messages)
        {
            return ParallelDataProcessing(DecryptMessage, messages);
        }

        /// <summary>
        ///     Gets encrypted input message.
        /// </summary>
        /// <param name="message">Message to encrypt.</param>
        /// <returns>Result of encrypting input message.</returns>
        public string EncryptMessage(string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                return string.Empty;
            }

            var result = new StringBuilder();
            var previousChar = default(char);
            foreach (var letter in message)
            {
                var button = keypad.GetButtonByValue(letter);
                if (button != null)
                {
                    if (button.Value.Contains(previousChar))
                    {
                        result.Append(SeparatorChar);
                    }

                    result.Append(button.GetCodeByLetter(letter));
                }
                else
                {
                    throw new ArgumentException($"Unknown letter : '{letter}'.");
                }

                previousChar = letter;
            }

            return result.ToString();
        }

        /// <summary>
        ///     Gets encrypted input messages.
        /// </summary>
        /// <param name="messages">Messages to encrypt.</param>
        /// <returns>Result of encrypting input messages.</returns>
        public List<string> EncryptMessages(List<string> messages)
        {
            return ParallelDataProcessing(EncryptMessage, messages);
        }

        /// <summary>
        ///     Parallels input method.
        /// </summary>
        /// <param name="methodToParallel">Method for paralleling data processing.</param>
        /// <param name="data">Input data for processing.</param>
        /// <returns>Processed data.</returns>
        private List<string> ParallelDataProcessing(Func<string, string> methodToParallel, IReadOnlyList<string> data)
        {
            var result = new List<string>(new string[data.Count]);
            Parallel.For(0, data.Count, i => result[i] = methodToParallel(data[i]));
            return result;
        }
    }
}