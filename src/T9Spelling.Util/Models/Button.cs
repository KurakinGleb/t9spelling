﻿namespace T9Spelling.Util.Models
{
    using System.Collections.ObjectModel;

    /// <summary>
    ///     Model for button.
    /// </summary>
    internal sealed class Button
    {
        /// <summary>
        ///     Key of the button.
        /// </summary>
        internal readonly char Key;

        /// <summary>
        ///     List of letters in the button.
        /// </summary>
        internal readonly ReadOnlyCollection<char> Value;

        /// <summary>
        ///     Initializes button.
        /// </summary>
        /// <param name="key">Key of the button.</param>
        /// <param name="value">Value of the button.</param>
        internal Button(char key, ReadOnlyCollection<char> value)
        {
            Key = key;
            Value = value;
        }

        /// <summary>
        ///     Gets code for the letter.
        /// </summary>
        /// <param name="letter">Letter getting code for.</param>
        /// <returns>Code of the input letter.</returns>
        internal string GetCodeByLetter(char letter)
        {
            return new string(Key, Value.IndexOf(letter) + 1);
        }
    }
}