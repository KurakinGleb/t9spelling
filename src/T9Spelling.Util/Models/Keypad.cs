﻿namespace T9Spelling.Util.Models
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Tools;

    /// <summary>
    ///     Model for keypad.
    /// </summary>
    public sealed class Keypad
    {
        /// <summary>
        ///     Initializes Keypad.
        /// </summary>
        /// <param name="keypad">List of symbols and its value which will be in keypad.</param>
        public Keypad(Dictionary<char, ReadOnlyCollection<char>> keypad)
        {
            foreach (var button in keypad)
            {
                AddButton(button.Key, button.Value);
            }
        }

        /// <summary>
        ///     English keypad.
        /// </summary>
        public static Keypad English => KeypadHelper.GetEnglishKeypad();

        /// <summary>
        ///     Russian keypad.
        /// </summary>
        public static Keypad Russian => KeypadHelper.GetRussianKeypad();

        /// <summary>
        ///     List of all buttons in the keypad.
        /// </summary>
        internal List<Button> Buttons { get; private set; }

        /// <summary>
        ///     Gets button with key like input char.
        /// </summary>
        /// <param name="key">Key with what trying to find button</param>
        /// <returns>Button with input chat in its key.</returns>
        internal Button GetButtonByKey(char key)
        {
            return Buttons.FirstOrDefault(b => b.Key == key);
        }

        /// <summary>
        ///     Gets button with input symbol in its value.
        /// </summary>
        /// <param name="value">Symbol in the buttons value.</param>
        /// <returns>Button with input value.</returns>
        internal Button GetButtonByValue(char value)
        {
            return Buttons.FirstOrDefault(b => b.Value.Contains(value));
        }

        /// <summary>
        ///     Adds new button.
        /// </summary>
        /// <param name="key">Key of the button to create.</param>
        /// <param name="value">Value of the button to create.</param>
        private void AddButton(char key, ReadOnlyCollection<char> value)
        {
            if (Buttons == null)
            {
                Buttons = new List<Button>();
            }

            if (Buttons.All(b => b.Key != key))
            {
                Buttons.Add(new Button(key, value));
            }
            else
            {
                throw new ArgumentException($"Failed to add '{key}' button. It already exists in keypad.");
            }
        }
    }
}