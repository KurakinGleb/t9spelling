﻿namespace T9Spelling.Util.Tools
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Models;

    /// <summary>
    ///     Helper for choosing keypad depending on language.
    /// </summary>
    public static class KeypadHelper
    {
        /// <summary>
        ///     English keypad.
        /// </summary>
        private static Keypad englishKeypad;

        /// <summary>
        ///     Russian keypad.
        /// </summary>
        private static Keypad russianKeypad;

        /// <summary>
        ///     Gets keypad depending on its language.
        /// </summary>
        /// <param name="language">Input language.</param>
        /// <returns>Keypad with input language.</returns>
        public static Keypad GetKeypadByLanguage(string language)
        {
            switch (language.ToLower())
            {
                case "en":
                case "english":
                    return GetEnglishKeypad();
                case "ru":
                case "russian":
                    return GetRussianKeypad();
            }

            throw new ArgumentException($"There is no '{language}' keypad.");
        }

        /// <summary>
        ///     Gets english keypad.
        /// </summary>
        /// <returns>Keypad with english language.</returns>
        internal static Keypad GetEnglishKeypad()
        {
            return englishKeypad ?? (englishKeypad = new Keypad(new Dictionary<char, ReadOnlyCollection<char>>
            {
                {'0', new List<char> {' '}.AsReadOnly()},
                {'2', new List<char> {'a', 'b', 'c'}.AsReadOnly()},
                {'3', new List<char> {'d', 'e', 'f'}.AsReadOnly()},
                {'4', new List<char> {'g', 'h', 'i'}.AsReadOnly()},
                {'5', new List<char> {'j', 'k', 'l'}.AsReadOnly()},
                {'6', new List<char> {'m', 'n', 'o'}.AsReadOnly()},
                {'7', new List<char> {'p', 'q', 'r', 's'}.AsReadOnly()},
                {'8', new List<char> {'t', 'u', 'v'}.AsReadOnly()},
                {'9', new List<char> {'w', 'x', 'y', 'z'}.AsReadOnly()}
            }));
        }

        /// <summary>
        ///     Gets russian keypad.
        /// </summary>
        /// <returns>Keypad with russian language.</returns>
        internal static Keypad GetRussianKeypad()
        {
            return russianKeypad ?? (russianKeypad = new Keypad(new Dictionary<char, ReadOnlyCollection<char>>
            {
                {'0', new List<char> {' '}.AsReadOnly()},
                {'2', new List<char> {'а', 'б', 'в', 'г'}.AsReadOnly()},
                {'3', new List<char> {'д', 'е', 'ж', 'з'}.AsReadOnly()},
                {'4', new List<char> {'и', 'й', 'к', 'л'}.AsReadOnly()},
                {'5', new List<char> {'м', 'н', 'о', 'п'}.AsReadOnly()},
                {'6', new List<char> {'р', 'с', 'т', 'у'}.AsReadOnly()},
                {'7', new List<char> {'ф', 'х', 'ц', 'ч'}.AsReadOnly()},
                {'8', new List<char> {'ш', 'щ', 'ъ', 'ы'}.AsReadOnly()},
                {'9', new List<char> {'ь', 'э', 'ю', 'я'}.AsReadOnly()}
            }));
        }
    }
}