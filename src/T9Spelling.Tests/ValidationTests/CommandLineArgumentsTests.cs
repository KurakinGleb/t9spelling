﻿namespace T9Spelling.Tests.ValidationTests
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Models;

    /// <summary>
    ///     Tests for right validation work of command line arguments.
    /// </summary>
    [TestClass]
    public class CommandLineArgumentsTests
    {
        [TestMethod]
        public void WrongInputFilePathTest()
        {
            //arrange
            var arguments = new CommandLineArguments(new[] {"-i", "ds.&//aq/sd"});
            var validationErrors = new List<ValidationResult>();
            var expectedValid = false;
            var expectedErrorText = "Input File doesn't exists.";
            var expectedNumberOfErrors = 1;

            //act
            var actual =
                Validator.TryValidateObject(arguments, new ValidationContext(arguments), validationErrors, true);

            //assert
            Assert.AreEqual(actual, expectedValid);
            Assert.AreEqual(validationErrors.Count, expectedNumberOfErrors);
            Assert.AreEqual(validationErrors[0].ErrorMessage, expectedErrorText);
        }

        [TestMethod]
        public void WrongOutputFilePathTest()
        {
            //arrange
            var arguments = new CommandLineArguments(new[] {"-o", "../asc/ascew/ds.&//aq/sd"});
            var validationErrors = new List<ValidationResult>();
            var expectedValid = false;
            var expectedErrorText = "Output File folder doesn't exists.";
            var expectedNumberOfErrors = 1;

            //act
            var actual =
                Validator.TryValidateObject(arguments, new ValidationContext(arguments), validationErrors, true);

            //assert
            Assert.AreEqual(actual, expectedValid);
            Assert.AreEqual(validationErrors.Count, expectedNumberOfErrors);
            Assert.AreEqual(validationErrors[0].ErrorMessage, expectedErrorText);
        }
    }
}