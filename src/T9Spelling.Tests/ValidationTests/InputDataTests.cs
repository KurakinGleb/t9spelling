﻿namespace T9Spelling.Tests.ValidationTests
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Core.DataProviders;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///     Tests for right validation work of input data.
    /// </summary>
    [TestClass]
    public class InputDataTests
    {
        [TestMethod]
        public void TwoBigMessageWasInputTest()
        {
            //arrange
            var fileProvider = new FileProvider("../../testData/testJsonTooBigMessage.json");
            var inputData = fileProvider.ReadData();
            var validationErrors = new List<ValidationResult>();
            var expectedValid = false;
            var expectedErrorText = "One or more messages has too big length.";
            var expectedNumberOfErrors = 1;

            //act
            var actual =
                Validator.TryValidateObject(inputData, new ValidationContext(inputData), validationErrors, true);
            var actualError = validationErrors[0].ErrorMessage;

            //assert
            Assert.AreEqual(actual, expectedValid);
            Assert.AreEqual(validationErrors.Count, expectedNumberOfErrors);
            Assert.AreEqual(actualError, expectedErrorText);
        }

        [TestMethod]
        public void WrongNumberInputTest()
        {
            //arrange
            var fileProvider = new FileProvider("../../testData/testJsonWrongNumber.json");
            var inputData = fileProvider.ReadData();
            var validationErrors = new List<ValidationResult>();
            var expectedValid = false;
            var expectedErrorText = "Input messages number and number of messages doesn't equal.";
            var expectedNumberOfErrors = 1;

            //act
            var actual =
                Validator.TryValidateObject(inputData, new ValidationContext(inputData), validationErrors, true);
            var actualError = validationErrors[0].ErrorMessage;

            //assert
            Assert.AreEqual(actual, expectedValid);
            Assert.AreEqual(validationErrors.Count, expectedNumberOfErrors);
            Assert.AreEqual(actualError, expectedErrorText);
        }
    }
}