﻿namespace T9Spelling.Tests.IntegrationTests
{
    using System.Collections.Generic;
    using Core;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///     Tests for right output data with different types of input after encrypting/decrypting.
    /// </summary>
    [TestClass]
    public class IntegrationTests
    {
        [TestMethod]
        public void CoderJsonTest()
        {
            //arrange
            var executor = new Executor(new[] {"-i", "../../testData/testJson.json"});

            //act
            executor.Run();

            //assert
            CollectionAssert.AreEqual(executor.OutputData.Messages, new List<string> {"4433555 555666096667775553"});
        }

        [TestMethod]
        public void CoderXmlTest()
        {
            //arrange
            var executor = new Executor(new[] {"-i", "../../testData/testXML.xml"});

            //act
            executor.Run();

            //assert
            CollectionAssert.AreEqual(executor.OutputData.Messages, new List<string> {"4433555 555666096667775553"});
        }

        [TestMethod]
        public void DeCoderJsonTest()
        {
            //arrange
            var executor = new Executor(new[] {"-i", "../../testData/testJsonDecoder.json", "-d"});

            //act
            executor.Run();

            //assert
            CollectionAssert.AreEqual(executor.OutputData.Messages, new List<string> {"hello world"});
        }

        [TestMethod]
        public void DeCoderXmlTest()
        {
            //arrange
            var executor = new Executor(new[] {"-i", "../../testData/testXMLDecoder.xml", "-d"});

            //act
            executor.Run();

            //assert
            CollectionAssert.AreEqual(executor.OutputData.Messages, new List<string> {"hello world"});
        }
    }
}