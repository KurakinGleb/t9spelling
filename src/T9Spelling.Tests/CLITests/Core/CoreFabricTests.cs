﻿namespace T9Spelling.Tests.CLITests.Core
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using T9Spelling.Core;
    using T9Spelling.Core.DataProviders;

    /// <summary>
    ///     Tests for right choosing writer and reader depending on input arguments.
    /// </summary>
    [TestClass]
    public class CoreFabricTests
    {
        [TestMethod]
        public void FabricForExecutorCliInputConsoleOutputTest()
        {
            //arrange
            var executor = new Executor(new[] {"-i", "input.json"});

            //act

            //assert
            Assert.IsInstanceOfType(executor.DataReader, typeof(FileProvider));
            Assert.IsInstanceOfType(executor.DataWriter, typeof(ConsoleProvider));
        }

        [TestMethod]
        public void FabricForExecutorConsoleInputCliOutputTest()
        {
            //arrange
            var executor = new Executor(new[] {"-o", "output.json"});

            //act

            //assert
            Assert.IsInstanceOfType(executor.DataReader, typeof(ConsoleProvider));
            Assert.IsInstanceOfType(executor.DataWriter, typeof(FileProvider));
        }

        [TestMethod]
        public void FabricForWorkerUiTest()
        {
            //arrange
            var executor = new Executor(new string[] { });

            //act

            //assert
            Assert.IsInstanceOfType(executor.DataReader, typeof(ConsoleProvider));
            Assert.IsInstanceOfType(executor.DataWriter, typeof(ConsoleProvider));
        }
    }
}