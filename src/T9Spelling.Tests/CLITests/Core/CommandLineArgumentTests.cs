﻿namespace T9Spelling.Tests.CLITests.Core
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using T9Spelling.Core;

    /// <summary>
    ///     Test for command line arguments.
    /// </summary>
    [TestClass]
    public class CommandLineArgumentTests
    {
        [TestMethod]
        public void InputFileSavingToArgsTest()
        {
            //arrange
            var executor = new Executor(new[] {"-i", "/testPath.json"});
            var expected = "/testPath.json";
            var actual = executor.CommandLineArguments.InputFile;

            //act

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsDecryptFalseToArgsTest()
        {
            //arrange
            var executor = new Executor(new[] {"-i", "/path.json"});
            var expected = false;
            var actual = executor.CommandLineArguments.IsDecrypt;

            //act

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsDecryptTrueToArgsTest()
        {
            //arrange
            var executor = new Executor(new[] {"-d"});
            var expected = true;
            var actual = executor.CommandLineArguments.IsDecrypt;

            //act

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsHelpFalseToArgsTest()
        {
            //arrange
            var executor = new Executor(new[] {"-i", "/path.xml", "-d"});
            var expected = false;
            var actual = executor.CommandLineArguments.IsHelp;

            //act

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsHelpTrueToArgsTest()
        {
            //arrange
            var executor = new Executor(new[] {"-h"});
            var expected = true;
            var actual = executor.CommandLineArguments.IsHelp;

            //act

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "The key {arg} is contained twice.")]
        public void KeyDoubleUsingTest()
        {
            //arrange
            var executor = new Executor(new[] {"-i", "b.xml", "-i", "a.xml"});

            //act
            executor.Run();
        }

        [TestMethod]
        public void LanguageSavingToArgsTest()
        {
            //arrange
            var executor = new Executor(new[] {"-l", "ru"});
            var expected = "ru";
            var actual = executor.CommandLineArguments.Language;

            //act

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void OutputFileSavingToArgsTest()
        {
            //arrange
            var executor = new Executor(new[] {"-o", "/testPath.xml"});
            var expected = "/testPath.xml";
            var actual = executor.CommandLineArguments.OutputFile;

            //act

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "The argument list must have the form (-key value).")]
        public void WrongUsingOfArgumentsTest()
        {
            //arrange
            var executor = new Executor(new[] {"i"});

            //act
            executor.Run();
        }
    }
}