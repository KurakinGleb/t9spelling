﻿namespace T9Spelling.Tests.CLITests.Core
{
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Json;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using T9Spelling.Core;
    using T9Spelling.Core.DataProviders;

    /// <summary>
    ///     Tests for right choosing serializers depending on the input file extension.
    /// </summary>
    [TestClass]
    public class FileProviderTests
    {
        [TestMethod]
        public void JsonSerializerForJsonFileTest()
        {
            //arrange
            var executor = new Executor(new[] {"-i", "input.json"});

            //act

            //assert
            Assert.IsInstanceOfType(((FileProvider) executor.DataReader).Serializer,
                typeof(DataContractJsonSerializer));
        }

        [TestMethod]
        public void XmlSerializerForJsonFileTest()
        {
            //arrange
            var executor = new Executor(new[] {"-i", "input.xml"});

            //act

            //assert
            Assert.IsInstanceOfType(((FileProvider) executor.DataReader).Serializer, typeof(DataContractSerializer));
        }
    }
}