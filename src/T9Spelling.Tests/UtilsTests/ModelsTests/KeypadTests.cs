﻿namespace T9Spelling.Tests.UtilsTests.ModelsTests
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Util;
    using Util.Models;
    using Util.Tools;

    /// <summary>
    ///     Tests with custom keypad.
    /// </summary>
    [TestClass]
    public class KeypadTests
    {
        private readonly Keypad testKeypad = new Keypad(
            new Dictionary<char, ReadOnlyCollection<char>>
            {
                {'0', new ReadOnlyCollection<char>(new List<char> {'a', 'b', 'c', 'd'})},
                {'1', new ReadOnlyCollection<char>(new List<char> {'e', 'f', 'g', 'h'})}
            });

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Failed to add '0' button. It already exists in keypad.")]
        public void ButtonsWithSameKeyTest()
        {
            //arrange
            var testKeypad = new Dictionary<char, ReadOnlyCollection<char>>
            {
                {'0', new ReadOnlyCollection<char>(new List<char> {' '})},
                {'0', new ReadOnlyCollection<char>(new List<char> {' '})}
            };

            //act

            //assert
            new Keypad(testKeypad);
        }

        [TestMethod]
        public void TestKeypadDecryptMessagesTest()
        {
            //arrange
            var testList = new List<string>
            {
                "011100",
                "1 1111000",
                "0",
                "111 111000 0000 01111 1"
            };
            var converter = new Converter(testKeypad);

            //act
            var actual = converter.DecryptMessages(testList);

            //assert
            var expected = new List<string>
            {
                "agb",
                "ehc",
                "a",
                "ggcdahe"
            };
            CollectionAssert.AreEqual(actual, expected);
        }

        [TestMethod]
        public void TestKeypadDecryptMessageTest()
        {
            //arrange
            var expected = "agb";
            var converter = new Converter(testKeypad);

            //act
            var actual = converter.DecryptMessage("011100");

            //assert
            Assert.AreEqual(actual, expected);
        }

        [TestMethod]
        public void TestKeypadEncryptMessagesTest()
        {
            //arrange
            var testList = new List<string>
            {
                "agb",
                "ehc",
                "a",
                "ggcdahe"
            };
            var converter = new Converter(testKeypad);

            //act
            var actual = converter.EncryptMessages(testList);

            //assert
            var expected = new List<string>
            {
                "011100",
                "1 1111000",
                "0",
                "111 111000 0000 01111 1"
            };
            CollectionAssert.AreEqual(actual, expected);
        }

        [TestMethod]
        public void TestKeypadEncryptMessageTest()
        {
            //arrange
            var expected = "011100";
            var converter = new Converter(testKeypad);

            //act
            var actual = converter.EncryptMessage("agb");

            //assert
            Assert.AreEqual(actual, expected);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "There is no 'Test' keypad.")]
        public void WrongKeypadLanguageTest()
        {
            //arrange

            //act

            //assert
            KeypadHelper.GetKeypadByLanguage("Test");
        }
    }
}