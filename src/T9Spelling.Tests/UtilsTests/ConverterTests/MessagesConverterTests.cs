﻿namespace T9Spelling.Tests.UtilsTests.ConverterTests
{
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Util;
    using Util.Models;

    /// <summary>
    ///     Tests for encrypting and decrypting list of messages.
    /// </summary>
    [TestClass]
    public class MessagesConverterTests
    {
        [TestMethod]
        public void TestListInputDataCodeTest()
        {
            //arrange
            var testList = new List<string>
            {
                "hi",
                "yes",
                "foo  bar",
                "hello world"
            };
            var converter = new Converter(Keypad.English);

            //act
            var actual = converter.EncryptMessages(testList);

            //assert
            var expected = new List<string>
            {
                "44 444",
                "999337777",
                "333666 6660 022 2777",
                "4433555 555666096667775553"
            };

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestListInputDataDecodeTest()
        {
            //arrange
            var testList = new List<string>
            {
                "44 444",
                "999337777",
                "333666 6660 022 2777",
                "4433555 555666096667775553"
            };

            var converter = new Converter(Keypad.English);

            //act
            var actual = converter.DecryptMessages(testList);

            //assert
            var expected = new List<string>
            {
                "hi",
                "yes",
                "foo  bar",
                "hello world"
            };

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}