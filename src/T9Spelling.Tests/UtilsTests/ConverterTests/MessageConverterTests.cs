﻿namespace T9Spelling.Tests.UtilsTests.ConverterTests
{
    using System;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Util;
    using Util.Models;

    /// <summary>
    ///     Tests for encrypting and decrypting one message.
    /// </summary>
    [TestClass]
    public class MessageConverterTests
    {
        [TestMethod]
        public void CodeMaxSymbolTest()
        {
            //arrange
            var converter = new Converter(Keypad.English);
            var testData = new string('t', 999);

            //act
            var actual = converter.EncryptMessage(testData);

            //assert
            var expected = string.Concat(Enumerable.Repeat("8 ", 999));
            expected = expected.Remove(expected.Length - 1);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void CodeOneSymbolRussianTest()
        {
            //arrange
            var converter = new Converter(Keypad.Russian);

            //act
            var actual = converter.DecryptMessage("2222");

            //assert
            var expected = "г";
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void CodeOneSymbolTest()
        {
            //arrange
            var converter = new Converter(Keypad.English);

            //act
            var actual = converter.DecryptMessage("222");

            //assert
            var expected = "c";
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void DecodeOneSymbolRussianTest()
        {
            //arrange
            var converter = new Converter(Keypad.Russian);

            //act
            var actual = converter.EncryptMessage("г");

            //assert
            var expected = "2222";
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void DecodeOneSymbolTest()
        {
            //arrange
            var converter = new Converter(Keypad.English);

            //act
            var actual = converter.EncryptMessage("c");

            //assert
            var expected = "222";
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void EmptyMessageTest()
        {
            //arrange
            var converter = new Converter(Keypad.English);

            //act
            var actual = converter.DecryptMessage(string.Empty);

            //assert
            var expected = string.Empty;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void NullMessageTest()
        {
            //arrange
            var converter = new Converter(Keypad.English);

            //act
            var actual = converter.DecryptMessage(string.Empty);

            //assert
            var expected = string.Empty;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Unknown digit : '.''")]
        public void UnexpectedDigitTest()
        {
            //arrange
            var converter = new Converter(Keypad.English);

            //act
            converter.EncryptMessage(".");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Unknown letter : '.''")]
        public void UnexpectedSymbolTest()
        {
            //arrange
            var converter = new Converter(Keypad.English);

            //act
            converter.DecryptMessage(".");
        }
    }
}