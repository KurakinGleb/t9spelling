﻿namespace T9Spelling.Models.BaseModels
{
    /// <summary>
    ///     Output data writer.
    /// </summary>
    internal interface IWritable
    {
        /// <summary>
        ///     Write output data.
        /// </summary>
        /// <param name="outputData">Output data.</param>
        void WriteData(Data outputData);
    }
}