﻿namespace T9Spelling.Models.BaseModels
{
    /// <summary>
    ///     Input data reader.
    /// </summary>
    internal interface IReadable
    {
        /// <summary>
        ///     Gets input data from current provider.
        /// </summary>
        /// <returns>Input data.</returns>
        Data ReadData();
    }
}