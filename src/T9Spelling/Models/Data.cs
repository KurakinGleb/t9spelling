﻿namespace T9Spelling.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.Serialization;
    using Tools;

    /// <summary>
    ///     Data model.
    /// </summary>
    [DataContract]
    internal sealed class Data : IValidatableObject
    {
        /// <summary>
        ///     Initializes data from list of messages.
        /// </summary>
        /// <param name="messages">List of input messages</param>
        internal Data(List<string> messages)
        {
            MessagesNumber = messages.Count;
            Messages = messages;
        }

        /// <summary>
        ///     List of messages.
        /// </summary>
        [DataMember]
        internal List<string> Messages { get; private set; }

        /// <summary>
        ///     Number of messages
        /// </summary>
        [DataMember]
        internal int MessagesNumber { get; private set; }

        /// <summary>
        ///     Validation of data object.
        /// </summary>
        /// <param name="validationContext">Validation context object.</param>
        /// <returns>List of errors after validation.</returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var errors = new List<ValidationResult>();
            var minMessagesNumber = Configuration.MinMessagesNumber;
            var maxMessagesNumber = Configuration.MaxMessagesNumber;
            var maxMessageLength = Configuration.MaxMessageLength;

            if (minMessagesNumber == null || maxMessagesNumber == null || maxMessageLength == null)
            {
                errors.Add(new ValidationResult("Some of variables 'MinMessagesNumber', 'MaxMessagesNumber', " +
                                                "'MaxMessageLength' are not set in config File."));
                return errors;
            }

            if (MessagesNumber < minMessagesNumber || MessagesNumber > maxMessagesNumber)
            {
                errors.Add(new ValidationResult("Wrong number of messages was entered."));
            }

            if (MessagesNumber != Messages.Count)
            {
                errors.Add(new ValidationResult("Input messages number and number of messages doesn't equal."));
            }

            foreach (var message in Messages)
            {
                if (string.IsNullOrEmpty(message))
                {
                    errors.Add(new ValidationResult("One or more messages are empty."));
                    continue;
                }

                if (message.Length > maxMessageLength)
                {
                    errors.Add(new ValidationResult("One or more messages has too big length."));
                }
            }

            return errors;
        }
    }
}