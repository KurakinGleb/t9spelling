﻿namespace T9Spelling.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.IO;

    /// <summary>
    ///     Model for parsing args from console.
    /// </summary>
    internal sealed class CommandLineArguments : IValidatableObject
    {
        private const string KeyStartingString = "-";

        /// <summary>
        ///     Dictionary for storing pairs of args name and value.
        /// </summary>
        private readonly Dictionary<string, string> arguments = new Dictionary<string, string>();

        /// <summary>
        ///     Initializes args from string array to argument object.
        /// </summary>
        /// <param name="args">Arguments from command line.</param>
        public CommandLineArguments(string[] args)
        {
            SetConsoleArguments(args);
        }

        /// <summary>
        ///     Name and path to file with input data.
        /// </summary>
        internal string InputFile => this["i"] ?? this["input"];

        /// <summary>
        ///     Flag for switching converting type.
        /// </summary>
        internal bool IsDecrypt => (this["d"] ?? this["decrypt"]) != null;

        /// <summary>
        ///     Flag for displaying help.
        /// </summary>
        internal bool IsHelp => (this["h"] ?? this["help"]) != null;

        /// <summary>
        ///     Indexer for parsed argument.
        /// </summary>
        /// <param name="key">Argument name.</param>
        /// <returns>Argument value.</returns>
        internal string this[string key] => !arguments.ContainsKey(key) ? null : arguments[key];

        /// <summary>
        ///     Flag for switching converting type.
        /// </summary>
        internal string Language => this["l"] ?? this["language"];

        /// <summary>
        ///     Name and path to file with output data.
        /// </summary>
        internal string OutputFile => this["o"] ?? this["output"];

        /// <summary>
        ///     Validation of data object.
        /// </summary>
        /// <param name="validationContext">Validation context object.</param>
        /// <returns>List of errors</returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var errors = new List<ValidationResult>();
            if (InputFile != null && !File.Exists(InputFile))
            {
                errors.Add(new ValidationResult("Input File doesn't exists."));
            }

            if (OutputFile != null && !Directory.Exists(new FileInfo(OutputFile).DirectoryName))
            {
                errors.Add(new ValidationResult("Output File folder doesn't exists."));
            }

            return errors;
        }

        /// <summary>
        ///     Gets help message string.
        /// </summary>
        /// <returns>Message for helping to work with CLI.</returns>
        internal static string GetHelpMessage()
        {
            return @"
USAGE:
    -h || [-i *path to File* | 
           -l [*en | ru*] | 
           -o *path* | -d ]

WHERE:
    >help(-h)           ... Show help message.
    >input(-i)          ... Path to input json/xml File.
    >language(-l)       ... Keypad language.
    >output(-o)         ... Path to output json/xml File.
    >decode(-d)         ... Decode input data to message.
";
        }

        /// <summary>
        ///     Check string is argument or argument's value.
        /// </summary>
        /// <param name="argument">Argument or argument value.</param>
        /// <returns>Checking if the value is key.</returns>
        private bool IsKey(string argument)
        {
            return argument.StartsWith(KeyStartingString, StringComparison.Ordinal);
        }

        /// <summary>
        ///     Sets arguments from console to console arguments object.
        /// </summary>
        /// <param name="args">Arguments from console.</param>
        private void SetConsoleArguments(string[] args)
        {
            for (var i = 0; i < args.Length; i++)
            {
                if (!args[i].StartsWith(KeyStartingString))
                {
                    throw new ArgumentException("The argument list must have the form (-key value).");
                }

                var arg = args[i].Remove(0, KeyStartingString.Length);
                if (arguments.ContainsKey(arg))
                {
                    throw new ArgumentException($"The key {arg} is contained more then once.");
                }

                var argValue = i == args.Length - 1 || IsKey(args[i + 1]) ? string.Empty : args[++i];
                arguments.Add(arg, argValue);
            }
        }
    }
}