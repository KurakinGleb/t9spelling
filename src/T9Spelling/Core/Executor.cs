﻿namespace T9Spelling.Core
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using Models;
    using Models.BaseModels;
    using Tools;
    using Util;
    using Util.Models;
    using Util.Tools;

    /// <summary>
    ///     Application executor.
    /// </summary>
    public sealed class Executor
    {
        /// <summary>
        ///     Arguments of the command line.
        /// </summary>
        internal readonly CommandLineArguments CommandLineArguments;

        /// <summary>
        ///     Initializes executor.
        /// </summary>
        /// <param name="args">Arguments for the executor.</param>
        public Executor(string[] args)
        {
            CommandLineArguments = new CommandLineArguments(args);
            DataReader = DataProviderHelper.GetDataReader(CommandLineArguments);
            DataWriter = DataProviderHelper.GetDataWriter(CommandLineArguments);
        }

        /// <summary>
        ///     Reads data.
        /// </summary>
        internal IReadable DataReader { get; }

        /// <summary>
        ///     Writes data.
        /// </summary>
        internal IWritable DataWriter { get; }

        /// <summary>
        ///     Data to Input.
        /// </summary>
        internal Data InpuData { get; private set; }

        /// <summary>
        ///     Data to output.
        /// </summary>
        internal Data OutputData { get; private set; }

        /// <summary>
        ///     Starts data processing and saves result.
        /// </summary>
        public void Run()
        {
            if (CommandLineArguments.IsHelp)
            {
                ConsoleHelper.PrintInfo(CommandLineArguments.GetHelpMessage());
                return;
            }

            if (!IsValid(CommandLineArguments))
            {
                return;
            }

            InpuData = DataReader.ReadData();
            if (!IsValid(InpuData))
            {
                return;
            }

            var converter = new Converter(CommandLineArguments.Language != null
                ? KeypadHelper.GetKeypadByLanguage(CommandLineArguments.Language)
                : Keypad.English);

            var decryptedMessages = CommandLineArguments.IsDecrypt
                ? converter.DecryptMessages(InpuData.Messages)
                : converter.EncryptMessages(InpuData.Messages);
            OutputData = new Data(decryptedMessages);

            DataWriter.WriteData(OutputData);
        }

        /// <summary>
        ///     Determines if the object is valid(doesn't have errors in itself).
        /// </summary>
        /// <param name="validatableObject">Object to validate</param>
        /// <returns>If the object is valid.</returns>
        private bool IsValid(IValidatableObject validatableObject)
        {
            var validationErrors = new List<ValidationResult>();
            if (Validator.TryValidateObject(validatableObject, new ValidationContext(validatableObject),
                validationErrors, true))
            {
                return true;
            }

            ConsoleHelper.PrintErrors(validationErrors.Select(e => e.ErrorMessage));
            return false;
        }
    }
}