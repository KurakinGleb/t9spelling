﻿namespace T9Spelling.Core.DataProviders
{
    using System;
    using System.IO;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Json;
    using Models;
    using Models.BaseModels;

    /// <summary>
    ///     Model for reading and writing data by file(xml/json).
    /// </summary>
    internal sealed class FileProvider : IReadable, IWritable
    {
        /// <summary>
        ///     Name and path to file.
        /// </summary>
        internal readonly string File;

        /// <summary>
        ///     File Serializer.
        /// </summary>
        internal readonly XmlObjectSerializer Serializer;

        /// <summary>
        ///     Initializes file data reader-writer.
        /// </summary>
        /// <param name="file">Name and path to file.</param>
        public FileProvider(string file)
        {
            Serializer = GetSerializer(file);
            File = file;
        }

        /// <inheritdoc />
        public Data ReadData()
        {
            using (var fs = new FileStream(File, FileMode.Open))
            {
                return (Data) Serializer.ReadObject(fs);
            }
        }

        /// <inheritdoc />
        public void WriteData(Data outputData)
        {
            using (var fs = new FileStream(File, FileMode.OpenOrCreate))
            {
                Serializer.WriteObject(fs, outputData);
            }
        }

        /// <summary>
        ///     Gets serializer depending on file extension.
        /// </summary>
        /// <param name="file">Name and path to file.</param>
        /// <returns>Serializer for the input file.</returns>
        private XmlObjectSerializer GetSerializer(string file)
        {
            var fileExtension = new FileInfo(file).Extension;
            switch (fileExtension)
            {
                case ".xml":
                    return new DataContractSerializer(typeof(Data));
                case ".json":
                    return new DataContractJsonSerializer(typeof(Data));
            }

            throw new ArgumentException($"Wrong File extension '{fileExtension}''.");
        }
    }
}