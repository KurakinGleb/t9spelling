﻿namespace T9Spelling.Core.DataProviders
{
    using System;
    using System.Collections.Generic;
    using Models;
    using Models.BaseModels;
    using Tools;

    /// <summary>
    ///     Model for reading and writing data by console.
    /// </summary>
    internal sealed class ConsoleProvider : IReadable, IWritable
    {
        /// <inheritdoc />
        public Data ReadData()
        {
            var number = GetMessagesNumber();
            var messages = GetMessages(number);
            return new Data(messages);
        }

        /// <inheritdoc />
        public void WriteData(Data outputData)
        {
            var i = 1;
            foreach (var message in outputData.Messages)
            {
                ConsoleHelper.PrintInfo($"Case #{i++}: {message}");
            }
        }

        /// <summary>
        ///     Gets input messages.
        /// </summary>
        /// <param name="messagesNumber">Number of input messages.</param>
        /// <returns>Input messages.</returns>
        private List<string> GetMessages(int messagesNumber)
        {
            ConsoleHelper.PrintInfo($"Please input {messagesNumber} messages :");
            var messages = new List<string>();
            for (var i = 0; i < messagesNumber; i++)
            {
                ConsoleHelper.PrintInfo($"#{i + 1}:");
                var message = Console.ReadLine();
                messages.Add(message);
            }

            return messages;
        }

        /// <summary>
        ///     Gets number of messages.
        /// </summary>
        /// <returns>Number of input messages.</returns>
        private int GetMessagesNumber()
        {
            ConsoleHelper.PrintInfo("Please input messages number:");
            var messagesNumberString = Console.ReadLine();
            if (!int.TryParse(messagesNumberString, out var messagesNumber))
            {
                throw new ArgumentException(
                    $"Wrong number of messages, failed to parse {messagesNumberString}");
            }

            return messagesNumber;
        }
    }
}