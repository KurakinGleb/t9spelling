namespace T9Spelling
{
    using System;
    using Core;
    using Tools;

    internal class Program
    {
        /// <summary>
        ///     The entry point.
        /// </summary>
        private static void Main(string[] args)
        {
            try
            {
                var executor = new Executor(args);
                executor.Run();
            }
            catch (Exception e)
            {
                ConsoleHelper.PrintError(e.InnerException?.Message ?? e.Message);
                // TODO: Add logger
            }
        }
    }
}