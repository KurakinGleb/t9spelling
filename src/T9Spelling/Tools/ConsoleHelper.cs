﻿namespace T9Spelling.Tools
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///     Helper for writing text in console.
    /// </summary>
    internal static class ConsoleHelper
    {
        /// <summary>
        ///     Prints error text with red color.
        /// </summary>
        /// <param name="message">Message to print.</param>
        internal static void PrintError(string message)
        {
            PrintText(message, ConsoleColor.Red);
        }

        /// <summary>
        ///     Prints list of errors.
        /// </summary>
        /// <param name="messages">Messages to print.</param>
        internal static void PrintErrors(IEnumerable<string> messages)
        {
            foreach (var message in messages)
            {
                PrintError(message);
            }
        }

        /// <summary>
        ///     Prints information text with blue color.
        /// </summary>
        /// <param name="message">Message to print.</param>
        internal static void PrintInfo(string message)
        {
            PrintText(message, ConsoleColor.Blue);
        }

        /// <summary>
        ///     Prints text
        /// </summary>
        /// <param name="message">Message to print.</param>
        /// <param name="color">Color of text to print.</param>
        private static void PrintText(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}