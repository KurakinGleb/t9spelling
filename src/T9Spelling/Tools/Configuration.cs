﻿namespace T9Spelling.Tools
{
    using System;
    using System.ComponentModel;
    using System.Configuration;

    /// <summary>
    ///     Configuration manager model.
    /// </summary>
    internal static class Configuration
    {
        /// <summary>
        ///     Maximum message length.
        /// </summary>
        internal static int? MaxMessageLength => GetConfigParam<int?>(ConfigurationManager.AppSettings.Get("MaxMessageLength"));

        /// <summary>
        ///     Maximum number of messages.
        /// </summary>
        internal static int? MaxMessagesNumber => GetConfigParam<int?>(ConfigurationManager.AppSettings.Get("MaxMessagesNumber"));

        /// <summary>
        ///     Minimum number of messages.
        /// </summary>
        internal static int? MinMessagesNumber => GetConfigParam<int?>(ConfigurationManager.AppSettings.Get("MinMessagesNumber"));

        private static T GetConfigParam<T>(string input)
        {
            try
            {
                var converter = TypeDescriptor.GetConverter(typeof(T));
                return (T)converter.ConvertFromString(input);
            }
            catch (Exception)
            {
                return default(T);
            }
        }
    }
}