﻿namespace T9Spelling.Tools
{
    using Core.DataProviders;
    using Models;
    using Models.BaseModels;

    /// <summary>
    ///     Helper for providing reader and writer.
    /// </summary>
    internal static class DataProviderHelper
    {
        /// <summary>
        ///     Gets data reader depending on existing of input file.
        /// </summary>
        /// <param name="commandLineArguments">Arguments from console.</param>
        /// <returns>Result data reader.</returns>
        internal static IReadable GetDataReader(CommandLineArguments commandLineArguments)
        {
            return commandLineArguments?.InputFile != null
                ? (IReadable) new FileProvider(commandLineArguments.InputFile)
                : new ConsoleProvider();
        }

        /// <summary>
        ///     Gets data writer depending on existing of output file.
        /// </summary>
        /// <param name="commandLineArguments">Arguments from console.</param>
        /// <returns>Result data writer.</returns>
        internal static IWritable GetDataWriter(CommandLineArguments commandLineArguments)
        {
            return commandLineArguments?.OutputFile != null
                ? (IWritable) new FileProvider(commandLineArguments.OutputFile)
                : new ConsoleProvider();
        }
    }
}